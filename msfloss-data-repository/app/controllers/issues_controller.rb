class IssuesController < ApplicationController
    @GET
    def index()
        render json: Issue.all
    end

    #POST
    def create()
        puts( "===================================================")
        puts "EmailsController.create: recebido(json): "+ params.to_json
        puts( "===================================================")


        # para validar schma, desabilitar

        #ref: https://github.com/ruby-json-schema/json-schema
        # is_valid_schema= JSON::Validator.validate('./app/schema/issue-schema.json', params.to_json, :fragment => "#/definitions/IssueRaw")
        # if !is_valid_schema 
        #     render json: {
        #         status: 400,
        #         message: "Formato inválido"
        #     }.to_json
        # end

        create_new_issue( params.to_unsafe_h)
        puts "registro salvo com sucesso!"
    end

    def create_new_issue( doc)
        #ref: https://stackoverflow.com/questions/30431059/uninitialized-constant-mongoconnection
        client = Mongo::Client.new([ 'localhost:27017' ], :database => 'msfloss')
        collection = client[:issue]
        result= collection.insert_one(doc)
        result.n
    end
end
